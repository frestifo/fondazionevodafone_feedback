package it.fondazionevodafone.feedback;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

public class CustomButton extends Button {

protected static String TEXT_STYLE_NORMAL = "normal", TEXT_STYLE_BOLD = "bold";
	
	public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.VodafoneFont);
        String fontFamily = null;
        int typeface = Typeface.NORMAL;
        final int n = a.getIndexCount();
        
        // get the attributes
        for (int i = 0; i < n; ++i) {
            int attr = a.getIndex(i);
            if (attr == R.styleable.VodafoneFont_android_fontFamily) {
                fontFamily = a.getString(attr);
            }
            else if (attr == R.styleable.VodafoneFont_android_textStyle) {
            	typeface = a.getInt(attr, Typeface.NORMAL);
            }
        }
        a.recycle();

        // apply them
        if (!isInEditMode()) {
            try {
            	if (fontFamily != null) {
	                Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontFamily);
	                setTypeface(tf, typeface);
            	}
            } 
            catch (Exception e) {
            	Log.e("CustomTextView", "Error setting typeface:\n" + e.toString());
            }
        }
    }
}
