
package it.fondazionevodafone.feedback;

import it.fondazionevodafone.feedback.minimaljson.JsonObject;
import it.fondazionevodafone.feedback.minimaljson.JsonValue;
import it.fondazionevodafone.feedback.minimaljson.ParseException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

/**
 * TODO
 * - add support for JSON response object
 */

public class NetworkRequest extends AsyncTask <String, Void, String> {

	private final static String TAG = "AsyncTask";
	protected final static String METHOD_GET = "GET", METHOD_POST = "POST";
		
	protected Block completionBlock;
	protected int readTimeout = 15000, connectionTimeout = 18000;	// milliseconds
	protected List<NameValuePair> parameters;
	protected String method = METHOD_GET;
	
	@Override
	protected void onPreExecute() {
	}
	
	@Override
	protected String doInBackground(String... urls) {
		// params comes from the execute() call: params[0] is the url.
		try {
			return downloadUrl(urls[0]);
		}
		catch (IOException e) {
			return "ERROR: Unable to retrieve web page. URL may be invalid.";
		}
	}
	
	@Override
	protected void onProgressUpdate (Void...progress) {
		
	}
	
	
	@Override 
	protected void onPostExecute(String result) {
		if (completionBlock != null)
			completionBlock.execute(result);
	}
	
	
	protected String downloadUrl(String urlString) throws IOException {
		String contentString = null; 
		InputStream stream = null;
		int length = 500;
		
		try {
			String parameterString = null;
			
			// POST REQUEST
			if (parameters != null) {
				parameterString = buildQueryString(parameters);
				if ( method.equalsIgnoreCase(METHOD_GET) )
					urlString = urlString + "?" + parameterString;
			}
			URL url = new URL(urlString);
			Log.v(TAG, method + " request to " + urlString);
			
			// create and activate the connection
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setReadTimeout(readTimeout);	
			connection.setConnectTimeout(connectionTimeout);
			connection.setRequestMethod(method);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			
			if ( method.equalsIgnoreCase(METHOD_POST) ) {
				OutputStream outputStream = connection.getOutputStream();
				BufferedWriter writer = new BufferedWriter( new OutputStreamWriter(outputStream, "UTF-8") );
				writer.write(parameterString);
				writer.close();
				outputStream.close();
			}
			
			connection.connect();
			
			
			// handle the response
			int responseCode = connection.getResponseCode();
			Log.v(TAG, "Response code: " + responseCode);
			if (responseCode >= 200 && responseCode <= 299) {
				stream = connection.getInputStream();
				contentString = readIt(stream, length);
			}
			
			return contentString;
		}
		// Something went wrong
		catch (Exception e) {
			Log.e(TAG, e.toString());
			return null;
		}
		// Clean-up
		finally {
			if (stream != null)
				stream.close();
		}
	}
	
	protected String buildQueryString(List<NameValuePair> params) throws UnsupportedEncodingException
	{
	    StringBuilder result = new StringBuilder();
	    boolean first = true;

	    for (NameValuePair pair : params)
	    {
	        if (first)
	            first = false;
	        else
	            result.append("&");

	        result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
	        result.append("=");
	        result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
	    }

	    return result.toString();
	}

	protected String readIt(InputStream stream, int length) throws IOException, UnsupportedEncodingException {
		
		String result = null;
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"), 8);
	    StringBuilder sb = new StringBuilder();

	    String line = null;
	    while ((line = reader.readLine()) != null)
	    {
	        sb.append(line + "\n");
	    }
	    result = sb.toString();
		
	    return result;
	}
	
	
	/**
	 * UTILITIES
	 */
	public static boolean isValidResponse(Object o, TextView errorText) {
		
		if (o == null) { 
			Log.w(TAG, "ERROR: invalid object received in server response");
			errorText.setText(R.string.serverError);
			return false;
		}
    	else if (!(o instanceof String)) {
			Log.w(TAG, "ERROR: invalid object received in server response: " + o.toString());
			errorText.setText(R.string.serverError);
			return false;
    	}
		return true;
	}

	public static String getStatus(String response) {
		
		try {
			JsonObject jsonObject = JsonObject.readFrom(response);
			JsonValue statusValue = jsonObject.get( "status" );
			if (statusValue != null) {
				Log.v(TAG, "status: " + statusValue.asString().trim());
				return statusValue.asString().trim();
			}
		}
		catch (ParseException e) {
			Log.e(TAG, "ERROR parsing server response:\n" + response + "\n" + e.toString());
		}
		return null;
	}
	
	public static Iterator<JsonValue> getParamIterator(String response, String paramName) {
		
		try {
			JsonObject jsonObject = JsonObject.readFrom(response);
			JsonValue value = jsonObject.get( paramName );
			if (value != null) {
				Log.v(TAG, paramName +": " + value.asArray().toString());
				return value.asArray().iterator();
			}
		}
		catch (ParseException e) {
			Log.e(TAG, "ERROR parsing server response:\n" + response + "\n" + e.toString());
		}
		return null;
	}

	public static void displayErrorText(String response, TextView errorText) {
		
		String error = null;
		JsonObject jsonObject = null;
		JsonValue errorValue = null;
		
		try {
			jsonObject = JsonObject.readFrom(response);
			errorValue = jsonObject.get( "error" );
		}
		catch (ParseException e) {
			Log.e(TAG, "ERROR parsing server response:\n" + response + "\n" + e.toString());
		}
		
		if (errorValue != null) {
			error = errorValue.asString().trim();
		}
		if (error != null) {
			errorText.setText(error);
			Log.v(TAG, "error: " + error);
		}
		else {
			errorText.setText(R.string.serverError);
		}
	}

	
	/**
	 * GETTERS AND SETTERS
	 */
	public Block getCompletionBlock() {
		return completionBlock;
	}

	public void setCompletionBlock(Block block) {
		this.completionBlock = block;
	}

	public int getReadTimeout() {
		return readTimeout;
	}

	public void setReadTimeout(int readTimeout) {
		this.readTimeout = readTimeout;
	}

	public int getConnectionTimeout() {
		return connectionTimeout;
	}

	public void setConnectionTimeout(int connectionTimeout) {
		this.connectionTimeout = connectionTimeout;
	}
	
	public List<NameValuePair> getParameters() {
		return parameters;
	}

	public void setParameters(List<NameValuePair> params) {
		this.parameters = params;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
}
