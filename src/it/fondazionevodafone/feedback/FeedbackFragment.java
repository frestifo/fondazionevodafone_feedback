package it.fondazionevodafone.feedback;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class FeedbackFragment extends DialogFragment {
	
	protected String message = null;

	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String text = (message != null) ? message : getString(R.string.form_toast);
        builder.setMessage(text)
               .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                       // go to the splash activity and show the home screen
                	   Intent intent = new Intent(getActivity(), Splash.class);
                   	   startActivity(intent);
                   
                   	   intent = new Intent(Intent.ACTION_MAIN);
	             	   intent.addCategory(Intent.CATEGORY_HOME);
	             	   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	             	   startActivity(intent);
                   }
               });
        // Create the AlertDialog object and return it
        return builder.create();
    }
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
