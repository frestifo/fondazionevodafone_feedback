package it.fondazionevodafone.feedback;

import it.fondazionevodafone.feedback.Login;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;


public class Registration extends Activity {

	private static final String TAG = "RegistrationActivity";
	protected final String SERVER_URL = "http://ascuoladiinternet.it/app/api"; //"http://192.168.0.5/API";
	protected static final String REGISTRATION_URL = "/register.php";
	protected static final String SERVER_RESPONSE_OK = "ok";
	
	protected EditText usernameEdit, classcodeEdit, zipcodeEdit;//, passwordEdit, confirmPasswordEdit;
	protected Spinner provinceSpinner;
	protected TextView errorText;
	protected CheckBox checkbox;
	ScrollView scrollView;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		// Show the Up button in the action bar.
		setupActionBar();
		
		// set instance variables
		classcodeEdit = (EditText)findViewById(R.id.reg_classcode);
		zipcodeEdit = (EditText)findViewById(R.id.reg_zipcode);
		usernameEdit = (EditText)findViewById(R.id.reg_username);
		//passwordEdit = (EditText)findViewById(R.id.reg_password);
		//confirmPasswordEdit = (EditText)findViewById(R.id.reg_confirmPassword);
		provinceSpinner = (Spinner)findViewById(R.id.reg_province);
		errorText = (TextView)findViewById(R.id.reg_errorText);
		checkbox = (CheckBox)findViewById(R.id.reg_checkbox);
		scrollView = (ScrollView)findViewById(R.id.reg_scrollView);
		
		checkbox.setChecked(true);
		
		// pre-populate the classcode field
		//SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		//String classcode = prefs.getString(Login.PREFS_CLASSCODE, ""); 
		//if (classcode.length() == 0)
			//Log.e(TAG, "Missing classcode in SharedPreferences");
		//else 
			//classcodeEdit.setText(classcode);
		
		// populate the province spinner:
		// Create an ArrayAdapter using the string array and a default spinner layout and
		// Specify the layout to use when the list of choices appears
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.array_province, R.layout.custom_spinner);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		provinceSpinner.setAdapter(adapter);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onRegister(View registerButton) {
		boolean missing = false;
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		
		errorText.setText("");
		
		// get all EditText child views, and check for their emptyness
		// also build parameters in the meantime
		ViewGroup group = (ViewGroup)findViewById(R.id.reg_linear); 
			
		for (int i = 0; i < group.getChildCount(); i++ ) {
			View child = group.getChildAt(i);
			if (child instanceof EditText) {
				missing = isEmpty((EditText)child);
				if (child.getTag() != null && (String.valueOf(child.getTag())).length() > 0)
					params.add(new BasicNameValuePair( String.valueOf(child.getTag()), String.valueOf(((EditText)child).getText())));
			}
		}
		
		// add Province (Spinner) and Zipcode (not in R.id.reg_linear) to params
		params.add(new BasicNameValuePair("province", provinceSpinner.getSelectedItem().toString()));
		params.add(new BasicNameValuePair("zip", String.valueOf(zipcodeEdit.getText())));
		params.add(new BasicNameValuePair("pass", "0000"));
		
		if (missing) {
			errorText.setText(getString(R.string.missingFields));
			//passwordEdit.setText("");
			//confirmPasswordEdit.setText("");
			scrollDown();
			return;
		}
		
		// check for a 5-digit zipcode
		if (String.valueOf(zipcodeEdit.getText()).matches("\\d{5}"))
			Log.v(TAG, "correct zipcode");
		else {
			Log.v(TAG, "incorrect zipcode");
			errorText.setText(R.string.reg_invalidZipcode);
			//passwordEdit.setText("");
			//confirmPasswordEdit.setText("");		
			scrollDown();
			return;
		}
		
		// check for mismatching passwords 
		/*
		if (String.valueOf(passwordEdit.getText()).compareTo( String.valueOf(confirmPasswordEdit.getText())) != 0) {
			errorText.setText(R.string.reg_passwMismatch);
			//passwordEdit.setText("");
			//confirmPasswordEdit.setText("");
			scrollDown();
			return;
		}
		*/
		
		// verify if the checkbox is checked
		if (!checkbox.isChecked()) {
			errorText.setText(R.string.reg_checkboxMissing);
			scrollDown();
			return;
		}

		// perform the request and get the response
		Log.v(TAG, "params:\n" + params.toString());
				
		NetworkRequest request = new NetworkRequest();
		request.setMethod("POST");
		request.setParameters(params);
		request.setCompletionBlock(new Block() {
			@Override
			public void execute(Object o) {
				onServerResponse(o);
			}
		});
		request.execute(SERVER_URL+REGISTRATION_URL);
	}
	
	protected boolean isEmpty(EditText edit) {
		if (edit.getText().length() == 0) {
			edit.setHintTextColor(getResources().getColor(R.color.red));
			//edit.setTypeface(edit.getTypeface(), Typeface.BOLD);
			return true;
		}
		return false;
	}
	
	protected void onServerResponse(Object o) {
		
		if (!NetworkRequest.isValidResponse(o, errorText)) {
			errorText.setText(R.string.serverError);
			scrollDown();
			return;
		}
		
		
		// status is ok: go to the Form activity
		String status = NetworkRequest.getStatus((String)o);
		if (status != null && status.trim().equalsIgnoreCase(SERVER_RESPONSE_OK)) {
			
			// save classcode and username to the preferences
			Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(this).edit();
			prefsEditor.putString(Login.PREFS_CLASSCODE, classcodeEdit.getText().toString());
			prefsEditor.putString(Login.PREFS_USERNAME, usernameEdit.getText().toString());
			//prefsEditor.putString(Login.PREFS_PASSWORD, passwordEdit.getText().toString());
			DateFormat dateFormat = DateFormat.getDateInstance();
			String date = dateFormat.format(new Date());
			prefsEditor.putString(Login.PREFS_DATE,  date);

			Log.d(TAG, prefsEditor.commit() ? "Preferences saved" : "ERROR SAVING PREFERENCES");
			prefsEditor.apply();
			
			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
			Log.d(TAG, prefs.getString(Login.PREFS_CLASSCODE, "No classcode found!")); 
			Log.d(TAG, prefs.getString(Login.PREFS_USERNAME, "No username found!"));
			//Log.d(TAG, prefs.getString(Login.PREFS_PASSWORD, "No password found!"));
			
			Log.d(TAG, classcodeEdit.getText().toString());
			Log.d(TAG, usernameEdit.getText().toString());
			//Log.d(TAG, passwordEdit.getText().toString());

			
			// go to the AfterRegistration activity
	    	Intent intent = new Intent(this, AfterRegistration.class);
	    	intent.putExtra(Login.PREFS_CLASSCODE, classcodeEdit.getText().toString());
	    	intent.putExtra(Login.PREFS_USERNAME, usernameEdit.getText().toString());
	    	//intent.putExtra(Login.PREFS_PASSWORD, passwordEdit.getText().toString());
	    	intent.putExtra(Login.PREFS_DATE, date);
	    	startActivity(intent);
		}
		// status is absent or ko: display an error message
		else {
			NetworkRequest.displayErrorText((String)o, errorText);
			
			//passwordEdit.setText("");
			//confirmPasswordEdit.setText("");
		}
	}
	
	protected void scrollDown() {
		scrollView .scrollTo(0, scrollView.getBottom());
	}
	
}
