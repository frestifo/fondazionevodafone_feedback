package it.fondazionevodafone.feedback;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class AfterRegistration extends Activity {
	
	private String username, /*password,*/ classcode, date;
	private static String TAG = "AfterRegistrationActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_after_registration);
		// Show the Up button in the action bar.
		setupActionBar();
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			classcode = extras.getString(Login.PREFS_CLASSCODE);
			username = extras.getString(Login.PREFS_USERNAME);
			//password = extras.getString(Login.PREFS_PASSWORD);
			date = extras.getString(Login.PREFS_DATE);
		}
		Log.d(TAG, classcode);
		Log.d(TAG, username);
		//Log.d(TAG, password);
		Log.d(TAG, date);
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.after_registration, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onFeedback(View button) {
		Intent intent = new Intent(this, Form.class);
		intent.putExtra(Login.PREFS_CLASSCODE, classcode);
    	intent.putExtra(Login.PREFS_USERNAME, username);
    	//intent.putExtra(Login.PREFS_PASSWORD, password);
    	intent.putExtra(Login.PREFS_DATE, date);
    	startActivity(intent);
	}
	
	public void onLastLesson(View button) {
		Intent intent = new Intent(this, FinalForm.class);
		intent.putExtra(Login.PREFS_CLASSCODE, classcode);
    	intent.putExtra(Login.PREFS_USERNAME, username);
    	//intent.putExtra(Login.PREFS_PASSWORD, password);
    	intent.putExtra(Login.PREFS_DATE, date);
    	startActivity(intent);
	}

}
