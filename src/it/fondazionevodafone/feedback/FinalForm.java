package it.fondazionevodafone.feedback;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.TargetApi;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

/*
 * TODO 
 * - impostare Status: 0 default, 1 al feedback di ultima lezione
 */
public class FinalForm extends ActionBarActivity {

	private static final String TAG = "FinalFormActivity";
	protected final String SERVER_URL = "http://ascuoladiinternet.it/app/api"; //"http://192.168.0.5/API";
	protected static final String FINAL_FEEDBACK_URL = "/finalfeedback.php";
	protected static final String SERVER_RESPONSE_OK = "ok";

	protected RadioButton radio1y, radio2y, radio3y, radio4y, radio5y;
	protected RadioButton radio1n, radio2n, radio3n, radio4n, radio5n;
	protected EditText comments1, comments2, comments3, comments4, comments5;
	protected TextView errorText;
	ScrollView scrollView;
	
	protected String _username = "", /*_password = "",*/ _classcode = "", _date = "";

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_final_form);
		// Show the Up button in the action bar.
		setupActionBar();
		
		radio1y = (RadioButton)findViewById(R.id.radio1_yes);
		radio2y = (RadioButton)findViewById(R.id.radio2_yes);
		radio3y = (RadioButton)findViewById(R.id.radio3_yes);
		radio4y = (RadioButton)findViewById(R.id.radio4_yes);
		radio5y = (RadioButton)findViewById(R.id.radio5_yes);
		radio1n = (RadioButton)findViewById(R.id.radio1_no);
		radio2n = (RadioButton)findViewById(R.id.radio2_no);
		radio3n = (RadioButton)findViewById(R.id.radio3_no);
		radio4n = (RadioButton)findViewById(R.id.radio4_no);
		radio5n = (RadioButton)findViewById(R.id.radio5_no);
		comments1 = (EditText)findViewById(R.id.fform_comments1);
		comments2 = (EditText)findViewById(R.id.fform_comments2);
		comments3 = (EditText)findViewById(R.id.fform_comments3);
		comments4 = (EditText)findViewById(R.id.fform_comments4);
		comments5 = (EditText)findViewById(R.id.fform_comments5);
		errorText = (TextView)findViewById(R.id.fform_errorText);
		scrollView = (ScrollView)findViewById(R.id.form_scrollView);
		
		
		Bundle extras = getIntent().getExtras();
		_classcode = extras.getString(Login.PREFS_CLASSCODE);
		_username = extras.getString(Login.PREFS_USERNAME);
		//_password = extras.getString(Login.PREFS_PASSWORD);
		_date = extras.getString(Login.PREFS_DATE);
	}
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.form, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	public void onSend(View sendButton) {
		
		errorText.setText("");
		
		if (!radio1y.isChecked() && ! radio1n.isChecked() || 
			!radio2y.isChecked() && ! radio2n.isChecked() || 
			!radio3y.isChecked() && ! radio3n.isChecked() || 
			!radio4y.isChecked() && ! radio4n.isChecked() || 
			!radio5y.isChecked() && ! radio5n.isChecked()) {
			errorText.setText(R.string.missingFields);
			return;
		}
		
		int radio1val = radio1y.isChecked() ? 1 : 0;
		int radio2val = radio2y.isChecked() ? 1 : 0;
		int radio3val = radio3y.isChecked() ? 1 : 0;
		int radio4val = radio4y.isChecked() ? 1 : 0;
		int radio5val = radio5y.isChecked() ? 1 : 0;
		String comment1 = comments1.getText().toString();
		String comment2 = comments2.getText().toString();
		String comment3 = comments3.getText().toString();
		String comment4 = comments4.getText().toString();
		String comment5 = comments5.getText().toString();
		
		
		// get preferences
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String classcode = prefs.getString(Login.PREFS_CLASSCODE, ""); 
		String username = prefs.getString(Login.PREFS_USERNAME, "");
		//String password = prefs.getString(Login.PREFS_PASSWORD, "");
		
		
		// fallback: bundle extras
		if (classcode.length() == 0 || username.length() == 0 || classcode.length() == 0) {
			classcode = _classcode;
			username = _username;
			//password = _password;
		}
				
		if (classcode.length() == 0 || username.length() == 0) {
			errorText.setText(R.string.form_missingCredentials);
			Log.e(TAG, "ERROR: missing classcode and/or username in SharedPreferences");
			scrollDown();
			return;
		}
		
		// check if all fields have been entered
		// N.B.: all conditions MUST be evaluated: do NOT use the short-circuit operator ||
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("class", classcode));
		params.add(new BasicNameValuePair("user", username));
		params.add(new BasicNameValuePair("pass", "0000"));
		params.add(new BasicNameValuePair("r1", String.valueOf(radio1val)));
		params.add(new BasicNameValuePair("r2", String.valueOf(radio2val)));
		params.add(new BasicNameValuePair("r3", String.valueOf(radio3val)));
		params.add(new BasicNameValuePair("r4", String.valueOf(radio4val)));
		params.add(new BasicNameValuePair("r5", String.valueOf(radio5val)));
		//if (comment1 != null && comment1.length() > 0)
			params.add(new BasicNameValuePair("c1", comment1));
		//if (comment2 != null && comment2.length() > 0)
			params.add(new BasicNameValuePair("c2", comment2));
		//if (comment3 != null && comment3.length() > 0)
			params.add(new BasicNameValuePair("c3", comment3));
		//if (comment4 != null && comment4.length() > 0)
			params.add(new BasicNameValuePair("c4", comment4));
		//if (comment5 != null && comment5.length() > 0)
			params.add(new BasicNameValuePair("c5", comment5));
		
		Log.v(TAG, "params:\n" + params.toString());
		
		NetworkRequest request = new NetworkRequest();
		request.setMethod("POST");
		request.setParameters(params);
		request.setCompletionBlock(new Block() {
			@Override
			public void execute(Object o) {
				onServerResponse(o);
			}
		});
		request.execute(SERVER_URL+FINAL_FEEDBACK_URL);
	}
	
	protected void onServerResponse(Object o) {
		
		if (!NetworkRequest.isValidResponse(o, errorText)) {
			errorText.setText(R.string.serverError);
			scrollDown();
			return;
		}
		
		// status is ok: delete preferences data, display a toast and exit the app
		String status = NetworkRequest.getStatus((String)o);
		if (status != null && status.trim().equalsIgnoreCase(SERVER_RESPONSE_OK)) {
			
			Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(this).edit();
			prefsEditor.remove(Login.PREFS_CLASSCODE);
			prefsEditor.remove(Login.PREFS_USERNAME);
			//prefsEditor.remove(Login.PREFS_PASSWORD);
			prefsEditor.remove(Login.PREFS_DATE);
			prefsEditor.commit();
			
			FeedbackFragment fragment = new FeedbackFragment();
			fragment.show(getSupportFragmentManager(), "FeedbackFragment");
		}
		// status is absent or ko: display an error message
		else {
			NetworkRequest.displayErrorText((String)o, errorText);
			
			if (errorText.getText().toString().length() == 0)
				errorText.setText(R.string.serverError);
			
			scrollDown();
		}
	}
	
	protected void scrollDown() {
		scrollView .scrollTo(0, scrollView.getBottom() - 300);
	}	
}
