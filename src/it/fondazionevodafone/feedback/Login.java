package it.fondazionevodafone.feedback;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;


public class Login extends Activity {

	private static final String TAG = "LoginActivity";
	protected final String SERVER_URL = "http://ascuoladiinternet.it/app/api"; //"http://192.168.0.5/API";
	protected static final String LOGIN_URL = "/login.php";
	protected static final String SERVER_RESPONSE_OK = "ok";
	public static final String PREFS_CLASSCODE  = "classcode";
	public static final String PREFS_USERNAME = "user";
	//public static final String PREFS_PASSWORD = "password";
	public static final String PREFS_DATE = "date";
	
	EditText classcodeEdit, usernameEdit;//, passwordEdit;
	TextView errorText;
	ScrollView scrollView;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		// Show the Up button in the action bar.
		setupActionBar();
		
		// set the instance variables corresponding to views declared in the layout file
		classcodeEdit = (EditText)findViewById(R.id.login_edit_classcode);
		usernameEdit = (EditText)findViewById(R.id.login_edit_username);
		//passwordEdit = (EditText)findViewById(R.id.login_edit_password);
		errorText = (TextView)findViewById(R.id.login_errorText);
		scrollView = (ScrollView)findViewById(R.id.login_scrollView);
		
		// set the keyboard Send action
		usernameEdit.setOnEditorActionListener(new OnEditorActionListener() {
		    @Override
		    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
		        boolean handled = false;
		        if (actionId == EditorInfo.IME_ACTION_SEND) {
		        	InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		        	imm.hideSoftInputFromWindow(usernameEdit.getWindowToken(), 0);
		            onLogin(usernameEdit);
		            handled = true;
		        }
		        return handled;
		    }
		});
		
		// remove preferences if they are older than one today
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String dateString = prefs.getString(Login.PREFS_DATE, "");
		if (dateString != null && dateString.length() > 0) {
			
			DateFormat dateFormat = DateFormat.getDateInstance();
			boolean remove = true;
			try {
				Date date = dateFormat.parse(dateString);
				if (date.compareTo(new Date()) != 0)	// do NOT remove preferences if they are NOT from today
					remove = false;
				
			} catch (ParseException e) {
				Log.w(TAG, "unable to parse PREFS_DATE entry: \n" + dateString);
				e.printStackTrace();
			}
			if (remove) {
				//Log.v(TAG, "removing preferences from " + dateString);
				//Editor prefsEditor = prefs.edit();
				//prefsEditor.remove(Login.PREFS_CLASSCODE);
				//prefsEditor.remove(Login.PREFS_USERNAME);
				//prefsEditor.remove(Login.PREFS_PASSWORD);
				//prefsEditor.remove(Login.PREFS_DATE);
				//prefsEditor.commit();
			}
		}
			
		// set the class code, if available, and focus on the next field
		//Intent intent = getIntent();
		//String classcode = intent.getStringExtra(Splash.EXTRA_CLASSCODE);
		//if (classcode != null) {
			//classcodeEdit.setText(classcode);
			//usernameEdit.requestFocus();
		//}
	}
	
	public void onRegister(View registerButton) {
		
		// go to the Registration activity
    	Intent intent = new Intent(this, Registration.class);
    	startActivity(intent);
	}

	public void onLogin(View sendButton) {
		
		errorText.setText("");
		
		// check if all fields have been entered
		// N.B.: all conditions MUST be evaluated: do NOT use the short-circuit operator ||
		if (isEmpty(classcodeEdit) | isEmpty(usernameEdit)) { // | isEmpty(passwordEdit)) {	
			errorText.setText(getString(R.string.missingFields));
			//passwordEdit.setText("");
			scrollDown();
			return;
		}
	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("class", String.valueOf(classcodeEdit.getText())));
		params.add(new BasicNameValuePair("user", String.valueOf(usernameEdit.getText())));
		params.add(new BasicNameValuePair("pass", "0000"));
		
		NetworkRequest request = new NetworkRequest();
		request.setMethod("POST");
		request.setParameters(params);
		request.setCompletionBlock(new Block() {
			@Override
			public void execute(Object o) {
				onServerResponse(o);
			}
		});
		request.execute(SERVER_URL+LOGIN_URL);
	}
	
	protected void scrollDown() {
		scrollView .scrollTo(0, scrollView.getBottom() - 300);
	}
	
	protected boolean isEmpty(EditText edit) {
		if (edit.getText().length() == 0) {
			edit.setHintTextColor(getResources().getColor(R.color.red));
			//edit.setTypeface(edit.getTypeface(), Typeface.BOLD);
			return true;
		}
		return false;
	}
	
	
	protected void onServerResponse(Object o) {
		
		if (!NetworkRequest.isValidResponse(o, errorText)) {
			errorText.setText(R.string.serverError);
			scrollDown();
			return;
		}
		
		// status is ok: go to the Form activity
		String status = NetworkRequest.getStatus((String)o);
		if (status != null && status.trim().equalsIgnoreCase(SERVER_RESPONSE_OK)) {
			// save classcode and username to the preferences
			Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(this).edit();
			prefsEditor.putString(PREFS_CLASSCODE, classcodeEdit.getText().toString());
			prefsEditor.putString(PREFS_USERNAME, usernameEdit.getText().toString());
			//prefsEditor.putString(PREFS_PASSWORD, passwordEdit.getText().toString());
			DateFormat dateFormat = DateFormat.getDateInstance();
			String date = dateFormat.format(new Date());
			prefsEditor.putString(PREFS_DATE,  date);
			
			
			
			Log.d(TAG, prefsEditor.commit() ? "Preferences saved" : "ERROR SAVING PREFERENCES");
			
			Log.d(TAG, getPreferences(MODE_PRIVATE).getString(PREFS_CLASSCODE, "No classcode found!"));
			Log.d(TAG, getPreferences(MODE_PRIVATE).getString(PREFS_USERNAME, "No username found!"));
			
			// go to the Form activity
	    	Intent intent = new Intent(this, Form.class);
	    	intent.putExtra(Login.PREFS_CLASSCODE, classcodeEdit.getText().toString());
	    	intent.putExtra(Login.PREFS_USERNAME, usernameEdit.getText().toString());
	    	//intent.putExtra(Login.PREFS_PASSWORD, passwordEdit.getText().toString());
	    	intent.putExtra(Login.PREFS_DATE, date);
	    	startActivity(intent);
		}
		// status is absent or ko: display an error message
		else {
			NetworkRequest.displayErrorText((String)o, errorText);
			
			if (errorText.getText().toString().length() == 0)
				errorText.setText(R.string.login_wrongCredentials);
			//passwordEdit.setText("");
			
			scrollDown();
		}
	}
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
