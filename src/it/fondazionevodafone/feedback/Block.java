package it.fondazionevodafone.feedback;

public interface Block {
	
	void execute(Object o);
}
