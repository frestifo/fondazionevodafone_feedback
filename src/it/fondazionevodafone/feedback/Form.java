package it.fondazionevodafone.feedback;

import it.fondazionevodafone.feedback.minimaljson.JsonObject;
import it.fondazionevodafone.feedback.minimaljson.JsonValue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;


public class Form extends ActionBarActivity {

	private static final String TAG = "FormActivity";
	protected final String SERVER_URL = "http://ascuoladiinternet.it/app/api"; //"http://192.168.0.5/API";
	protected static final String LESSONS_URL = "/getlessons.php";
	protected static final String FEEDBACK_URL = "/feedback.php";
	protected static final String SERVER_RESPONSE_OK = "ok";

	protected RatingBar ratingBar;
	protected EditText commentsEdit;
	protected TextView errorText;
	ScrollView scrollView;
	Spinner spinner;
	
	protected String username, /*password,*/ classcode, date;
	protected String _username = "", /*_password = "",*/ _classcode = "", _date = "";
	protected ArrayList<String> lezioni;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_form);
		// Show the Up button in the action bar.
		setupActionBar();
		
		ratingBar = (RatingBar)findViewById(R.id.form_ratingBar);
		commentsEdit = (EditText)findViewById(R.id.form_commentsEdit);
		errorText = (TextView)findViewById(R.id.form_errorText);
		scrollView = (ScrollView)findViewById(R.id.form_scrollView);
		spinner = (Spinner)findViewById(R.id.form_spinner);
		
		// get the extras from the bundle
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			_classcode = extras.getString(Login.PREFS_CLASSCODE);
			_username = extras.getString(Login.PREFS_USERNAME);
			//_password = extras.getString(Login.PREFS_PASSWORD);
			_date = extras.getString(Login.PREFS_DATE);
		}
		Log.d(TAG, _classcode);
		Log.d(TAG, _username);
		//Log.d(TAG, _password);
		Log.d(TAG, _date);
		
		getLessons();
	}
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.form, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void getLessons() {

		// get the classcode
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String classcode = prefs.getString(Login.PREFS_CLASSCODE, ""); 
		if (classcode.length() == 0)
			classcode = _classcode;
		
		// compute the parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("class", classcode));
		
		Log.v(TAG, "params:\n" + params.toString());
		
		// send the request
		NetworkRequest request = new NetworkRequest();
		request.setMethod("POST");
		request.setParameters(params);
		request.setCompletionBlock(new Block() {
			@Override
			public void execute(Object o) {
				onLessonsReceived(o);
			}
		});
		request.execute(SERVER_URL+LESSONS_URL);
	}
	
	protected void onLessonsReceived(Object o) {
		
		if (!NetworkRequest.isValidResponse(o, errorText)) {
			errorText.setText(R.string.serverError);
			scrollDown();
			return;
		}
		
		// status is ok: delete preferences data, display a toast and exit the app
		String status = NetworkRequest.getStatus((String)o);
		if (status != null && status.trim().equalsIgnoreCase(SERVER_RESPONSE_OK)) {
			
			lezioni = new ArrayList<String>();

			// get the values out of the array
			Iterator<JsonValue> iterator = NetworkRequest.getParamIterator((String)o, "lezioni");
			List<String> stringList = new ArrayList<String>();
			while(iterator.hasNext()) {
		         JsonObject jsonValue = (JsonObject)iterator.next();
		         String id = jsonValue.get("id").toString();
		         String title = jsonValue.get("titolo").asString();
		         String date = jsonValue.get("data").asString();
		         
		         stringList.add(title + " - " + date);
		         
		         lezioni.add(id);
		    }
			
			// set up the spinner
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.custom_spinner, stringList);
			spinner.setAdapter(adapter);
			
			// open the spinner
			spinner.performClick();
		}
		// status is absent or ko: display an error message
		else {
			NetworkRequest.displayErrorText((String)o, errorText);
			
			if (errorText.getText().toString().length() == 0)
				errorText.setText(R.string.serverError);
			
			scrollDown();
		}
	}
	
	
	public void onSend(View sendButton) {
		
		errorText.setText("");
		
		int rating = (int) ratingBar.getRating();
		String comments = commentsEdit.getText().toString();
		
		if (rating == 0) {
			errorText.setText(R.string.form_missingRating);
			scrollDown();
			return;
		}
		
		// get preferences
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		classcode = prefs.getString(Login.PREFS_CLASSCODE, ""); 
		username = prefs.getString(Login.PREFS_USERNAME, "");
		//password = prefs.getString(Login.PREFS_PASSWORD, "");
		
		// fallback: bundle extras
		if (classcode.length() == 0 | username.length() == 0 | classcode.length() == 0) {
			classcode = _classcode;
			username = _username;
			//password = _password;
		}
		
		if (classcode.length() == 0 | username.length() == 0 | classcode.length() == 0) {
			errorText.setText(R.string.form_missingCredentials);
			Log.e(TAG, "ERROR: missing classcode, username and/or password in SharedPreferences and bundle extras");
			scrollDown();
			return;
		}
		
		// check if all fields have been entered
		// N.B.: all conditions MUST be evaluated: do NOT use the short-circuit operator ||
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("time", String.valueOf(System.currentTimeMillis() / 1000L)));
		params.add(new BasicNameValuePair("class", classcode));
		params.add(new BasicNameValuePair("user", username));
		params.add(new BasicNameValuePair("pass", "0000"));
		params.add(new BasicNameValuePair("rating", String.valueOf(rating)));
		if (comments != null && comments.length() > 0)
			params.add(new BasicNameValuePair("text", String.valueOf(comments)));
		
		// get the spinner's selected item, and get the idLezione from there
		int index = spinner.getSelectedItemPosition();
		if (index == AdapterView.INVALID_POSITION || index > lezioni.size() - 1) {
			errorText.setText(R.string.form_missingLesson);
			return;
		}
		String idLezione = lezioni.get(index);
		params.add(new BasicNameValuePair("lesson", idLezione));
		
		Log.v(TAG, "params:\n" + params.toString());
		
		NetworkRequest request = new NetworkRequest();
		request.setMethod("POST");
		request.setParameters(params);
		request.setCompletionBlock(new Block() {
			@Override
			public void execute(Object o) {
				onServerResponse(o);
			}
		});
		request.execute(SERVER_URL+FEEDBACK_URL);
	}
	
	protected void onServerResponse(Object o) {
		
		if (!NetworkRequest.isValidResponse(o, errorText)) {
			errorText.setText(R.string.serverError);
			scrollDown();
			return;
		}
		
		// status is ok: delete preferences data, display a toast and exit the app
		String status = NetworkRequest.getStatus((String)o);
		if (status != null && status.trim().equalsIgnoreCase(SERVER_RESPONSE_OK)) {
			
			//Editor prefsEditor = PreferenceManager.getDefaultSharedPreferences(this).edit();
			//prefsEditor.remove(Login.PREFS_CLASSCODE);
			//prefsEditor.remove(Login.PREFS_USERNAME);
			//prefsEditor.remove(Login.PREFS_PASSWORD);
			//prefsEditor.remove(Login.PREFS_DATE);
			//prefsEditor.commit();
			
			// if the user rated the last lession, show the FinalForm
			int index = spinner.getSelectedItemPosition();
			if (index != AdapterView.INVALID_POSITION && index == lezioni.size() - 1) {
				Intent intent = new Intent(this, FinalForm.class);
				intent.putExtra(Login.PREFS_CLASSCODE, classcode);
		    	intent.putExtra(Login.PREFS_USERNAME, username);
		    	//intent.putExtra(Login.PREFS_PASSWORD, password);
		    	intent.putExtra(Login.PREFS_DATE, date);
		    	startActivity(intent);
			}
			// else show the FeedbackFragment and close the app
			else {
				FeedbackFragment fragment = new FeedbackFragment();
				fragment.show(getSupportFragmentManager(), "FeedbackFragment");
			}
		}
		// status is absent or ko: display an error message
		else {
			NetworkRequest.displayErrorText((String)o, errorText);
			
			if (errorText.getText().toString().length() == 0)
				errorText.setText(R.string.serverError);
			
			scrollDown();
		}
	}
	
	protected void scrollDown() {
		scrollView .scrollTo(0, scrollView.getBottom() - 300);
	}	
	
	
	public void onLastLesson(View button) {
		// go to the FinalForm activity
		Intent intent = new Intent(this, FinalForm.class);
		intent.putExtra(Login.PREFS_CLASSCODE, classcode);
    	intent.putExtra(Login.PREFS_USERNAME, username);
    	//intent.putExtra(Login.PREFS_PASSWORD, password);
    	intent.putExtra(Login.PREFS_DATE, date);
    	startActivity(intent);
	}
	
}
