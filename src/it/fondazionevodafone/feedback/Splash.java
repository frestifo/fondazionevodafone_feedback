package it.fondazionevodafone.feedback;

import it.fondazionevodafone.feedback.minimaljson.JsonObject;
import it.fondazionevodafone.feedback.minimaljson.ParseException;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;


public class Splash extends FragmentActivity {
	
	// constants
	private final static String TAG = "SplashActivity";
	public final static String EXTRA_CLASSCODE = "it.fondazionevodafone.feedback.CLASSCODE";
	protected final String SERVER_URL = "http://ascuoladiinternet.it/app/api"; //"http://192.168.0.5/API";
	protected final String REQUEST_URL = "/findClass.php";
	protected final int DELAY = 2000;	// milliseconds //TODO: SET TO 10 SECONDS
	
	// instance variables
	protected String responseString, classcode;
	protected LocationListener locationListener;
	protected Location location;
	Handler handler;
	Runnable runnable;

	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.splash, menu);
        return true;
    }
    
    
    @Override
    protected void onResume() {
    	super.onResume();
    	
    	ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo info = manager.getActiveNetworkInfo();
    	
    	// internet connection available
    	if (info != null && info.isConnected()) {
    		// do nothing: wait for the timer to fire
    	}
    	// offline: do not wait for timer or location, go to the next activity instead
    	else {
    		Log.v(TAG, "No network connection available");
    		FeedbackFragment fragment = new FeedbackFragment();
    		fragment.setMessage(getString(R.string.splash_toast));
			fragment.show(getSupportFragmentManager(), "OfflineFragment");
    		return;
    	}
        
        // LOCATION
        startListeningToLocationUpdates();
        
        // start the timer: wait for DELAY milliseconds to get a location fix, then perform the request 
        handler = new Handler();
        runnable = new Runnable() {            
            @Override
            public void run() {
                performRequest();                  
            }
        };
        handler.postDelayed(runnable, DELAY);
    }
    
    
    protected void performRequest() {
    	LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
    	
    	// prepare request parameters
    	long timestamp = System.currentTimeMillis() / 1000L;
    	if (location == null)
    		location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		if (location == null)
			location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
    	
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("time", String.valueOf(timestamp)));
		if (location != null) {
			params.add(new BasicNameValuePair("lat", String.valueOf(location.getLatitude())));
			params.add(new BasicNameValuePair("lat", String.valueOf(location.getLongitude())));
		}
		Log.v(TAG, "parameters: " + params.toString()); 
		
    	// perform the request
		NetworkRequest request = new NetworkRequest();
		request.setMethod("GET");
		request.setParameters(params);
		request.setCompletionBlock(new Block() {
			@Override
			public void execute(Object o) {
				onServerResponse(o);
			}
		});
		request.execute(SERVER_URL+REQUEST_URL);
    }
    
    // executed when the Send button is clicked
    protected void onServerResponse(Object o) {
    	
    	if (o == null) { 
			Log.w(TAG, "ERROR: invalid object sent to SplashActivity");
		}
    	else if (!(o instanceof String)) {
			Log.w(TAG, "ERROR: invalid object sent to SplashActivity: " + o.toString());
    	}
    	else {
    		// parse the server response into JSONObject's
    		responseString = ((String)o);
    		Log.v(TAG, "response:\n" + responseString);
    		
    		try {
	    		JsonObject jsonObject = JsonObject.readFrom(responseString);
	    		classcode = jsonObject.get( "classcode" ).asString().trim();
	    		if (classcode != null && classcode.length() > 0)
	    			Log.v(TAG, "Classcode: "+ classcode);
    		}
    		catch (ParseException e) {
    			Log.e(TAG, "ERROR: response not a JSON string");
    		}
    		catch (UnsupportedOperationException e) {
    			Log.e(TAG, "ERROR: ill-formed response received from server");
    		}
    	}
    	
		goToNextActivity();
    }
    
    protected void goToNextActivity() {
    	
    	// Remove the previously added listener
    	if (locationListener != null) {
	    	LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
	    	locationManager.removeUpdates(locationListener);
    	}
    	
    	// remove any pending callbacks, if any
    	if (handler != null)
    		handler.removeCallbacks(runnable);
    	
    	// go to the Login activity
    	Intent intent = new Intent(this, Login.class);
    	if (classcode != null && classcode.length() > 0)
    		intent.putExtra(EXTRA_CLASSCODE, classcode.trim());
    	startActivity(intent);
    }
    
    
    /**
     * LOCATION
     */
    protected void startListeningToLocationUpdates() {
    	
    	// Acquire a reference to the system Location Manager
    	LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

    	// Define a listener that responds to location updates
    	locationListener = new LocationListener() {
    	    public void onLocationChanged(Location newLocation) {
    	      if (isBetterLocation(newLocation, location)) {
    	    	  location = newLocation;
    	    	  Log.v(TAG, "Received better location:\n" + location.toString());
    	      }
    	      else 
    	    	  Log.v(TAG, "Received equal or worse location:\n" + location.toString());
    	    }

    	    public void onStatusChanged(String provider, int status, Bundle extras) {}

    	    public void onProviderEnabled(String provider) {}

    	    public void onProviderDisabled(String provider) {}
    	  };

    	// Register the listener with the Location Manager to receive location updates
    	locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, locationListener);
    	locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, locationListener);
    }
    
    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /** Determines whether one Location reading is better than the current Location fix
      * @param location  The new Location that you want to evaluate
      * @param currentBestLocation  The current Location fix, to which you want to compare the new one
      */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
        // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
          return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}
